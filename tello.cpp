#include "tello.h"
#include "assert.h"

#include <string>

#include <fmt/core.h>
#include <fmt/format.h>

void Tello::init() {
    res.reserve(2048);
    sendsock = socket(AF_INET, SOCK_DGRAM, 0);
    recvsock = socket(AF_INET, SOCK_DGRAM, 0);
    videosock = socket(AF_INET, SOCK_DGRAM, 0);
    dest.sin_family = AF_INET;
    dest.sin_port = htons(port);
    dest.sin_addr.s_addr = inet_addr(host.c_str());
    src.sin_family = AF_INET;
    src.sin_port = htons(recvport);
    src.sin_addr.s_addr = inet_addr(recvhost.c_str());
    srcvideo.sin_family = AF_INET;
    srcvideo.sin_port = htons(recvvideoport);
    srcvideo.sin_addr.s_addr = inet_addr(recvhost.c_str());

    assert(bind(sendsock, (struct sockaddr*)&dest, sizeof(dest)) != -1);
    assert(bind(recvsock, (struct sockaddr*)&src, sizeof(src)) != -1);
    assert(bind(videosock, (struct sockaddr*)&srcvideo, sizeof(srcvideo)) != -1);
}

u32 Tello::send_pkt(const std::string& content) {
    u32 bytes = sendto(sendsock,
            content.c_str(),
            content.length(),
            0,
            (struct sockaddr*)&dest, 
            sizeof(dest));
    return bytes;
}

u32 Tello::recv_pkt() {
    socklen_t srclenaddrlen = sizeof(src);
    u32 bytes = recvfrom(recvsock,
        (void*)res.c_str(),
        res.capacity(),
        0,
        (struct sockaddr*)&src, 
        &srclenaddrlen);
    return bytes;
}

u32 Tello::recv_video_pkt() {
    socklen_t srclenaddrlen = sizeof(srcvideo);
    u32 bytes = recvfrom(videosock,
        (void*)vstream_h264,
        65536,
        0,
        (struct sockaddr*)&srcvideo,
        &srclenaddrlen);
    return bytes;
}

u32 Tello::connect() {
    return send_pkt("command");
}

u32 Tello::takeoff() {
    return send_pkt("takeoff");
}

u32 Tello::land() {
    return send_pkt("land");
}

u32 Tello::streamon() {
    return send_pkt("streamon");
}

u32 Tello::streamoff() {
    return send_pkt("streamoff");
}

u32 Tello::emergency() {
    return send_pkt("emergency");
}

u32 Tello::stop() {
    return send_pkt("stop");
}

u32 Tello::up(u32 x) {
    assert(x >= 20 && x <= 500);
    return send_pkt(fmt::format("up {}", x));
}

u32 Tello::down(u32 x) {
    assert(x >= 20 && x <= 500);
    return send_pkt(fmt::format("down {}", x));
}

u32 Tello::right(u32 x) {
    assert(x >= 20 && x <= 500);
    return send_pkt(fmt::format("right {}", x));
}

u32 Tello::left(u32 x) {
    assert(x >= 20 && x <= 500);
    return send_pkt(fmt::format("left {}", x));
}

u32 Tello::forward(u32 x) {
    assert(x >= 20 && x <= 500);
    return send_pkt(fmt::format("forward {}", x));
}

u32 Tello::back(u32 x) {
    assert(x >= 20 && x <= 500);
    return send_pkt(fmt::format("back {}", x));
}

u32 Tello::cw(u32 x) {
    assert(x >= 1 && x <= 360);
    return send_pkt(fmt::format("cw {}", x));
}

u32 Tello::ccw(u32 x) {
    assert(x >= 1 && x <= 360);
    return send_pkt(fmt::format("ccw {}", x));
}

u32 Tello::flip(FlipDirection fd) {
    return send_pkt(fmt::format("flip {}", static_cast<char>(fd)));
}

u32 Tello::go(u32 x, u32 y, u32 z, u32 speed) {
    assert(x >= -500 && x <= 500);
    assert(y >= -500 && y <= 500);
    assert(z >= -500 && z <= 500);
    assert(speed >= 10 && speed <= 100);
    assert(x <= -20 && x >= 20 || y <= -20 && y >= 20 || z <= -20 && z >= 20);
    return send_pkt(fmt::format("go {} {} {} {}", x, y, z, speed));
}

u32 Tello::go(u32 x, u32 y, u32 z, u32 speed, u32 mid) {
    assert(x >= -500 && x <= 500);
    assert(y >= -500 && y <= 500);
    assert(z >= -500 && z <= 500);
    assert(mid >= 1 && mid <= 8);
    assert(speed >= 10 && speed <= 100);
    assert(x <= -20 && x >= 20 || y <= -20 && y >= 20 || z <= -20 && z >= 20);
    return send_pkt(fmt::format("go {} {} {} {} m{}", x, y, z, speed, mid));
}

u32 Tello::curve(u32 x1, u32 y1, u32 z1, u32 x2, u32 y2, u32 z2, u32 speed) {
    assert(x1 >= -500 && x1 <= 500);
    assert(y1 >= -500 && y1 <= 500);
    assert(z1 >= -500 && z1 <= 500);
    assert(x2 >= -500 && x2 <= 500);
    assert(y2 >= -500 && y2 <= 500);
    assert(z2 >= -500 && z2 <= 500);
    assert(speed >= 10 && speed <= 100);
    assert(x1 <= -20 && x1 >= 20 || y1 <= -20 && y1 >= 20 || z1 <= -20 && z1 >= 20);
    assert(x2 <= -20 && x2 >= 20 || y2 <= -20 && y2 >= 20 || z2 <= -20 && z2 >= 20);
    return send_pkt(fmt::format("curve {} {} {} {} {} {} {}", x1, y1, z1, x2, y2, z2, speed));
}

u32 Tello::curve(u32 x1, u32 y1, u32 z1, u32 x2, u32 y2, u32 z2, u32 speed, u32 mid) {
    assert(x1 >= -500 && x1 <= 500);
    assert(y1 >= -500 && y1 <= 500);
    assert(z1 >= -500 && z1 <= 500);
    assert(x2 >= -500 && x2 <= 500);
    assert(y2 >= -500 && y2 <= 500);
    assert(z2 >= -500 && z2 <= 500);
    assert(mid >= 1 && mid <= 8);
    assert(speed >= 10 && speed <= 60);
    assert(x1 <= -20 && x1 >= 20 || y1 <= -20 && y1 >= 20 || z1 <= -20 && z1 >= 20);
    assert(x2 <= -20 && x2 >= 20 || y2 <= -20 && y2 >= 20 || z2 <= -20 && z2 >= 20);
    return send_pkt(fmt::format("curve {} {} {} {} {} {} {} m{}", x1, y1, z1, x2, y2, z2, speed, mid));
}

u32 Tello::jump(u32 x, u32 y, u32 z, u32 speed, u32 yaw, u32 mid1, u32 mid2) {
    assert(x >= -500 && x <= 500);
    assert(y >= -500 && y <= 500);
    assert(z >= -500 && z <= 500);
    assert(mid1 >= 1 && mid1 <= 8);
    assert(mid2 >= 1 && mid2 <= 8);
    assert(mid1 != mid2);
    assert(speed >= 10 && speed <= 100);
    assert(x <= -20 && x >= 20 || y <= -20 && y >= 20 || z <= -20 && z >= 20);
    return send_pkt(fmt::format("jump {} {} {} {} {} m{} m{}", x, y, z, speed, yaw, mid1, mid2));
}

u32 Tello::speed(u32 x) {
    assert(x >= 10 && x <= 100);
    return send_pkt(fmt::format("speed {}", x));
}

u32 Tello::rc(u32 a, u32 b, u32 c, u32 d) {
    assert(a >= -100 && a <= 100);
    assert(b >= -100 && b <= 100);
    assert(c >= -100 && c <= 100);
    assert(d >= -100 && d <= 100);
    return send_pkt(fmt::format("rc {} {} {} {}", a, b, c, d));
}

u32 Tello::wifi(const std::string& ssid, const std::string pwd) {
    return send_pkt(fmt::format("wifi {} {}", ssid, pwd));
}

u32 Tello::mon() {
    return send_pkt("mon");
}

u32 Tello::moff() {
    return send_pkt("moff");
}

u32 Tello::mdirection(MDirection md) {
    return send_pkt(fmt::format("mdirection {}", static_cast<u8>(md)));
}

u32 Tello::ap(const std::string& ssid, const std::string pwd) {
    return send_pkt(fmt::format("ap {} {}", ssid, pwd));
}

u32 Tello::r_speed() {
    u32 bytes = send_pkt("speed?");
    recv_pkt();
    return bytes;
}

u32 Tello::r_battery() {
    u32 bytes = send_pkt("battery?");
    recv_pkt();
    return bytes;
}

u32 Tello::r_time() {
    u32 bytes = send_pkt("time?");
    recv_pkt();
    return bytes;
}

u32 Tello::r_wifi() {
    u32 bytes = send_pkt("wifi?");
    recv_pkt();
    return bytes;
}

u32 Tello::r_sdk() {
    u32 bytes = send_pkt("sdk?");
    recv_pkt();
    return bytes;
}

u32 Tello::r_sn() {
    u32 bytes = send_pkt("sn?");
    recv_pkt();
    return bytes;
}
