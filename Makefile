CC = g++
CXXFLAGS = -std=c++2a -O2 -g -Wall
SRCS = main.cpp tello.cpp
OBJS = $(SRCS:.cpp=.o)
EXECS = main

main: $(OBJS) -lfmt

clean:
	@rm -f $(EXECS) $(OBJS)
