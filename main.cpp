#include <stdio.h>

#include <cstdlib>
#include <string>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fmt/core.h>

#include "tello.h"
#include "ds4.h"

#define TELLO_START -500.0f
#define TELLO_END 500.0f
#define TELLO_INTERP(value, start, end) \
            (TELLO_START + ((value - start)/(end - start)) * (TELLO_END - TELLO_START))

static
float tello_ds4_interp(float ds4axis_value) { return TELLO_INTERP(ds4axis_value, DS4::AXIS_START, DS4::AXIS_END); }

static void loop(DS4& pad, Tello& tello) {
    while (true) {
        pad.poll_event();
        if (pad.btn_pressed(DS4_CIRCLE)) {
            fmt::print("Circle pressed!\n");
        }
        if (pad.btn_pressed(DS4_CROSS)) {
            fmt::print("Cross pressed!\n");
        }
        if (pad.btn_pressed(DS4_SQUARE)) {
            fmt::print("Square pressed!\n");
        }
        if (pad.btn_pressed(DS4_TRIANGLE)) {
            fmt::print("Triangle pressed!\n");
        }
    }
}

int main(void)
{
    DS4 pad;
    Tello tello;

    //tello.init();
    //tello.connect();
    
    loop(pad, tello);

    return 0;
}
