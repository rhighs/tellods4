#pragma once

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <unordered_map>
#include <linux/joystick.h>

#include <map>
#include <functional>

#include "types.h"

typedef u8 DS4Event;
#define DS4_CROSS       0
#define DS4_CIRCLE      1
#define DS4_TRIANGLE    2
#define DS4_SQUARE      3
#define DS4_L1          4
#define DS4_R1          5
#define DS4_L2          6
#define DS4_R2          7
#define DS4_SHARE       8
#define DS4_OPTIONS     9
#define DS4_PS          10
#define DS4_L3          11
#define DS4_R3          12
#define AXIS_DS4_LX     20
#define AXIS_DS4_LY     21
#define AXIS_DS4_RX     23
#define AXIS_DS4_RY     24
#define AXIS_DS4_L2     22
#define AXIS_DS4_R2     25
#define NEVENTS         26

static const char* evt2str(DS4Event evt) {
    switch (evt) {
    case DS4_CROSS:     return "DS4_CROSS";
    case DS4_CIRCLE:    return "DS4_CIRCLE";
    case DS4_TRIANGLE:  return "DS4_TRIANGLE";
    case DS4_SQUARE:    return "DS4_SQUARE";
    case DS4_L1:        return "DS4_L1";
    case DS4_R1:        return "DS4_R1";
    case DS4_L2:        return "DS4_L2";
    case DS4_R2:        return "DS4_R2";
    case DS4_SHARE:     return "DS4_SHARE";
    case DS4_OPTIONS:   return "DS4_OPTIONS";
    case DS4_PS:        return "DS4_PS";
    case DS4_L3:        return "DS4_L3";
    case DS4_R3:        return "DS4_R3";
    case AXIS_DS4_LX:   return "AXIS_DS4_LX";
    case AXIS_DS4_LY:   return "AXIS_DS4_LY";
    case AXIS_DS4_RX:   return "AXIS_DS4_RX";
    case AXIS_DS4_RY:   return "AXIS_DS4_RY";
    case AXIS_DS4_L2:   return "AXIS_DS4_L2";
    case AXIS_DS4_R2:   return "AXIS_DS4_R2";
    }
}

int read_event(int fd, struct js_event *event) {
    ssize_t bytes;
    bytes = read(fd, event, sizeof(*event));
    if (bytes == sizeof(*event))
        return 0;
    return -1;
}

struct DS4 {
    static constexpr float AXIS_START = -32767.0f;
    static constexpr float AXIS_END = 32767.0f;
    float states[NEVENTS];

    i32 js;
    const char* device = "/dev/input/js0";

    DS4() {
        for (u8 i = 0; i < NEVENTS; i++)
            states[i] = 0.0f;
        js = open(device, O_RDONLY);
        if (js == -1) {
            perror("Could not open joystick");
        }
    }

    bool btn_pressed(DS4Event e) const {
        assert(e < AXIS_DS4_LX && "Can't use an axis as a button event");
        return states[e] > 0.9;
    }

    float axis_value(DS4Event e) const {
        assert(e >= AXIS_DS4_LX && "Can't use a button to get an axis value");
        return states[e];
    }

    void poll_event() {
        struct js_event e;
        if (read_event(js, &e) != 0) return;
        switch (e.type) {
        case JS_EVENT_BUTTON: {
            u8 button = e.number;
            states[button] = e.value;
            break;
        }
        case JS_EVENT_AXIS: {
            u8 axis = e.number + AXIS_DS4_LX;
            states[axis] = e.value;
            break;
        }
        }
    }
};
