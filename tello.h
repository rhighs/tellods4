#pragma once
#include <stdio.h>

#include <cstdlib>
#include <string>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "types.h"

enum class FlipDirection : char {
    Left = 'l',
    Right = 'r',
    Forward = 'f',
    Backward = 'b'
};

enum class MDirection : u8 {
    Downward, Forward, Both
};

struct Tello {
    std::string host = "192.168.10.1";
    std::string recvhost = "0.0.0.0";
    u32 port = 8889;
    u32 recvport = 8890;
    u32 recvvideoport = 11111;
    sockaddr_in dest;
    sockaddr_in src;
    sockaddr_in srcvideo;
    std::string res;
    u32 sendsock;
    u32 recvsock;
    u32 videosock;
    u32 vstream_h264[65536];

    ~Tello() { close(sendsock); close(recvsock); close(videosock); }
    void init();
    u32 send_pkt(const std::string& content);
    u32 recv_pkt();
    u32 recv_video_pkt();

    u32 connect();
    u32 takeoff();
    u32 land();
    u32 streamon();
    u32 streamoff();
    u32 emergency();
    u32 stop();
    u32 up(u32 x);
    u32 down(u32 x);
    u32 right(u32 x);
    u32 left(u32 x);
    u32 forward(u32 x);
    u32 back(u32 x);
    u32 cw(u32 x);
    u32 ccw(u32 x);
    u32 flip(FlipDirection fd);
    u32 go(u32 x, u32 y, u32 z, u32 speed);
    u32 go(u32 x, u32 y, u32 z, u32 speed, u32 mid);
    u32 curve(u32 x1, u32 y1, u32 z1, u32 x2, u32 y2, u32 z2, u32 speed);
    u32 curve(u32 x1, u32 y1, u32 z1, u32 x2, u32 y2, u32 z2, u32 speed, u32 mid);
    u32 jump(u32 x, u32 y, u32 z, u32 speed, u32 yaw, u32 mid1, u32 mid2);
    u32 speed(u32 x);
    u32 rc(u32 a, u32 b, u32 c, u32 d);
    u32 wifi(const std::string& ssid, const std::string pwd);
    u32 mon();
    u32 moff();
    u32 mdirection(MDirection md);
    u32 ap(const std::string& ssid, const std::string pwd);
    u32 r_speed();
    u32 r_battery();
    u32 r_time();
    u32 r_wifi();
    u32 r_sdk();
    u32 r_sn();
};
